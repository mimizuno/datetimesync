package datetimesync

import (
	"bitbucket.org/mimizuno/datetimesync/proto"
	"syscall"
	"time"
	"unsafe"

	"github.com/golang/protobuf/ptypes"
)

//go:generate protoc --go_out=plugins=grpc:. ./proto/*.proto

var (
	kernel32 = syscall.NewLazyDLL("kernel32.dll")

	procSetSystemTime = kernel32.NewProc("SetSystemTime")
	procGetSystemTime = kernel32.NewProc("GetSystemTime")
)

// SYSTEMTIME from http://msdn.microsoft.com/en-us/library/windows/desktop/ms724950(v=vs.85).aspx
type SYSTEMTIME struct {
	Year         uint16
	Month        uint16
	DayOfWeek    uint16 // 0: Sunday, 6:Saturday
	Day          uint16
	Hour         uint16
	Minute       uint16
	Second       uint16
	Milliseconds uint16 // 0-999
}

func toWin32ApiStruct(s proto.SystemTime) SYSTEMTIME {
	t, _ := ptypes.Timestamp(s.Time)
	return SYSTEMTIME{
		Year:         uint16(t.Year()),
		Month:        uint16(t.Month()),
		DayOfWeek:    uint16(t.Weekday()),
		Day:          uint16(t.Day()),
		Hour:         uint16(t.Hour()),
		Minute:       uint16(t.Minute()),
		Second:       uint16(t.Second()),
		Milliseconds: uint16(t.Nanosecond() / 1000 / 1000),
	}
}

func fromWin32ApiStruct(s SYSTEMTIME) proto.SystemTime {
	t := time.Time{}
	t = t.AddDate(int(s.Year), int(s.Month), int(s.Day)).Add(time.Hour*time.Duration(s.Hour) + time.Minute*time.Duration(s.Minute) + time.Second*time.Duration(s.Second) + time.Millisecond*time.Duration(s.Milliseconds))

	p, _ := ptypes.TimestampProto(t)
	return proto.SystemTime{
		Time: p,
	}
}

// SetSystemTime sets the current system time and date. The system time is UTC.
func SetSystemTime(s proto.SystemTime) bool {
	t := toWin32ApiStruct(s)
	ret, _, _ := procSetSystemTime.Call(uintptr(unsafe.Pointer(&t)))
	return ret != 0
}

// GetSystemTime retrives the current system date and time. The system time is UTC.
func GetSystemTime() proto.SystemTime {
	var time SYSTEMTIME
	procGetSystemTime.Call(uintptr(unsafe.Pointer(&time)))
	return fromWin32ApiStruct(time)
}
