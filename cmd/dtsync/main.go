package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"bitbucket.org/mimizuno/datetimesync"
	pb "bitbucket.org/mimizuno/datetimesync/proto"
	"strings"

	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc"
)

var option struct {
	Address  string
	IsServer bool
}

const (
	defaultAddressPort = ":5000"
)

func init() {
	flag.StringVar(&option.Address, "address", defaultAddressPort, "Server address [host]:port")
	flag.BoolVar(&option.IsServer, "server", false, "run as server mode")
}

func main() {
	flag.Parse()
	if option.IsServer {
		runAsServer()
	} else {
		runAsClient()
	}
}

func runAsClient() {
	if strings.HasPrefix(option.Address, ":") {
		option.Address = "localhost" + option.Address
	} else if !strings.Contains(option.Address, ":") {
		option.Address = option.Address + defaultAddressPort
	}

	conn, err := grpc.Dial(option.Address, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
		return
	}
	defer conn.Close()

	c := pb.NewSystemTimeSyncClient(conn)

	now := datetimesync.GetSystemTime()
	resp, err := c.Ping(context.Background(), &now)
	if err != nil {
		log.Fatal(err)
	}
	current := datetimesync.GetSystemTime()
	fmt.Println(resp, &current)
	sentTime, _ := ptypes.Timestamp(now.Time)
	receivedTime, _ := ptypes.Timestamp(current.Time)
	diff := receivedTime.Sub(sentTime) / 2
	fmt.Println(diff)

	nowTime, _ := ptypes.Timestamp(datetimesync.GetSystemTime().Time)
	now.Time, _ = ptypes.TimestampProto(nowTime.Add(diff))

	c.SetTime(context.Background(), &now)
}

func runAsServer() {
	fmt.Printf("Start listening at %v\n", option.Address)
	lis, err := net.Listen("tcp", option.Address)
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Println("Start grpc server")
	s := grpc.NewServer()
	pb.RegisterSystemTimeSyncServer(s, &server{})
	s.Serve(lis)
}

type server struct{}

func (s *server) Ping(ctx context.Context, in *pb.SystemTime) (*pb.SystemTime, error) {
	fmt.Println("Ping", in.Time)
	out := *in
	return &out, nil
}

func (s *server) SetTime(ctx context.Context, in *pb.SystemTime) (*pb.SystemTime, error) {
	fmt.Println("SetTime", in.Time)
	datetimesync.SetSystemTime(*in)
	out := datetimesync.GetSystemTime()
	fmt.Println("NowTime", out.Time)
	return &out, nil
}
