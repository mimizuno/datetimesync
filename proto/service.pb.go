// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/service.proto

/*
Package proto is a generated protocol buffer package.

It is generated from these files:
	proto/service.proto

It has these top-level messages:
	SystemTime
	SystemTimeRaw
*/
package proto

import proto1 "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import google_protobuf "github.com/golang/protobuf/ptypes/timestamp"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto1.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto1.ProtoPackageIsVersion2 // please upgrade the proto package

type SystemTime struct {
	Time *google_protobuf.Timestamp `protobuf:"bytes,1,opt,name=Time" json:"Time,omitempty"`
}

func (m *SystemTime) Reset()                    { *m = SystemTime{} }
func (m *SystemTime) String() string            { return proto1.CompactTextString(m) }
func (*SystemTime) ProtoMessage()               {}
func (*SystemTime) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *SystemTime) GetTime() *google_protobuf.Timestamp {
	if m != nil {
		return m.Time
	}
	return nil
}

type SystemTimeRaw struct {
	Year         int32 `protobuf:"varint,1,opt,name=Year" json:"Year,omitempty"`
	Month        int32 `protobuf:"varint,2,opt,name=Month" json:"Month,omitempty"`
	DayOfWeek    int32 `protobuf:"varint,3,opt,name=DayOfWeek" json:"DayOfWeek,omitempty"`
	Day          int32 `protobuf:"varint,4,opt,name=Day" json:"Day,omitempty"`
	Hour         int32 `protobuf:"varint,5,opt,name=Hour" json:"Hour,omitempty"`
	Minute       int32 `protobuf:"varint,6,opt,name=Minute" json:"Minute,omitempty"`
	Second       int32 `protobuf:"varint,7,opt,name=Second" json:"Second,omitempty"`
	Milliseconds int32 `protobuf:"varint,8,opt,name=Milliseconds" json:"Milliseconds,omitempty"`
}

func (m *SystemTimeRaw) Reset()                    { *m = SystemTimeRaw{} }
func (m *SystemTimeRaw) String() string            { return proto1.CompactTextString(m) }
func (*SystemTimeRaw) ProtoMessage()               {}
func (*SystemTimeRaw) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *SystemTimeRaw) GetYear() int32 {
	if m != nil {
		return m.Year
	}
	return 0
}

func (m *SystemTimeRaw) GetMonth() int32 {
	if m != nil {
		return m.Month
	}
	return 0
}

func (m *SystemTimeRaw) GetDayOfWeek() int32 {
	if m != nil {
		return m.DayOfWeek
	}
	return 0
}

func (m *SystemTimeRaw) GetDay() int32 {
	if m != nil {
		return m.Day
	}
	return 0
}

func (m *SystemTimeRaw) GetHour() int32 {
	if m != nil {
		return m.Hour
	}
	return 0
}

func (m *SystemTimeRaw) GetMinute() int32 {
	if m != nil {
		return m.Minute
	}
	return 0
}

func (m *SystemTimeRaw) GetSecond() int32 {
	if m != nil {
		return m.Second
	}
	return 0
}

func (m *SystemTimeRaw) GetMilliseconds() int32 {
	if m != nil {
		return m.Milliseconds
	}
	return 0
}

func init() {
	proto1.RegisterType((*SystemTime)(nil), "proto.SystemTime")
	proto1.RegisterType((*SystemTimeRaw)(nil), "proto.SystemTimeRaw")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for SystemTimeSync service

type SystemTimeSyncClient interface {
	Ping(ctx context.Context, in *SystemTime, opts ...grpc.CallOption) (*SystemTime, error)
	SetTime(ctx context.Context, in *SystemTime, opts ...grpc.CallOption) (*SystemTime, error)
}

type systemTimeSyncClient struct {
	cc *grpc.ClientConn
}

func NewSystemTimeSyncClient(cc *grpc.ClientConn) SystemTimeSyncClient {
	return &systemTimeSyncClient{cc}
}

func (c *systemTimeSyncClient) Ping(ctx context.Context, in *SystemTime, opts ...grpc.CallOption) (*SystemTime, error) {
	out := new(SystemTime)
	err := grpc.Invoke(ctx, "/proto.SystemTimeSync/Ping", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *systemTimeSyncClient) SetTime(ctx context.Context, in *SystemTime, opts ...grpc.CallOption) (*SystemTime, error) {
	out := new(SystemTime)
	err := grpc.Invoke(ctx, "/proto.SystemTimeSync/SetTime", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for SystemTimeSync service

type SystemTimeSyncServer interface {
	Ping(context.Context, *SystemTime) (*SystemTime, error)
	SetTime(context.Context, *SystemTime) (*SystemTime, error)
}

func RegisterSystemTimeSyncServer(s *grpc.Server, srv SystemTimeSyncServer) {
	s.RegisterService(&_SystemTimeSync_serviceDesc, srv)
}

func _SystemTimeSync_Ping_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SystemTime)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SystemTimeSyncServer).Ping(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.SystemTimeSync/Ping",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SystemTimeSyncServer).Ping(ctx, req.(*SystemTime))
	}
	return interceptor(ctx, in, info, handler)
}

func _SystemTimeSync_SetTime_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SystemTime)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SystemTimeSyncServer).SetTime(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.SystemTimeSync/SetTime",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SystemTimeSyncServer).SetTime(ctx, req.(*SystemTime))
	}
	return interceptor(ctx, in, info, handler)
}

var _SystemTimeSync_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.SystemTimeSync",
	HandlerType: (*SystemTimeSyncServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Ping",
			Handler:    _SystemTimeSync_Ping_Handler,
		},
		{
			MethodName: "SetTime",
			Handler:    _SystemTimeSync_SetTime_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/service.proto",
}

func init() { proto1.RegisterFile("proto/service.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 275 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x90, 0xcf, 0x4a, 0xc3, 0x40,
	0x10, 0x87, 0xad, 0x6d, 0x5a, 0x1d, 0xff, 0xa0, 0xa3, 0xc8, 0x12, 0x04, 0x25, 0x27, 0x4f, 0x1b,
	0xac, 0x57, 0x8f, 0x3d, 0x78, 0x09, 0x4a, 0x52, 0x10, 0x8f, 0x69, 0x9c, 0xc6, 0xc5, 0x24, 0x5b,
	0xb2, 0x1b, 0x25, 0x8f, 0xe9, 0x1b, 0xc9, 0xce, 0x5a, 0x82, 0x78, 0xf1, 0xb4, 0xf3, 0x7d, 0x33,
	0xbf, 0x81, 0x1d, 0x38, 0xdb, 0xb4, 0xda, 0xea, 0xd8, 0x50, 0xfb, 0xa1, 0x0a, 0x92, 0x4c, 0x18,
	0xf0, 0x13, 0x5e, 0x95, 0x5a, 0x97, 0x15, 0xc5, 0x4c, 0xab, 0x6e, 0x1d, 0x5b, 0x55, 0x93, 0xb1,
	0x79, 0xbd, 0xf1, 0x73, 0xd1, 0x3d, 0x40, 0xd6, 0x1b, 0x4b, 0xf5, 0x52, 0xd5, 0x84, 0x12, 0x26,
	0xee, 0x15, 0xa3, 0xeb, 0xd1, 0xcd, 0xc1, 0x3c, 0x94, 0x3e, 0x2d, 0xb7, 0x69, 0xb9, 0xdc, 0xa6,
	0x53, 0x9e, 0x8b, 0xbe, 0x46, 0x70, 0x34, 0xc4, 0xd3, 0xfc, 0x13, 0x11, 0x26, 0x2f, 0x94, 0xb7,
	0xbc, 0x21, 0x48, 0xb9, 0xc6, 0x73, 0x08, 0x12, 0xdd, 0xd8, 0x37, 0xb1, 0xcb, 0xd2, 0x03, 0x5e,
	0xc2, 0xfe, 0x22, 0xef, 0x1f, 0xd7, 0xcf, 0x44, 0xef, 0x62, 0xcc, 0x9d, 0x41, 0xe0, 0x09, 0x8c,
	0x17, 0x79, 0x2f, 0x26, 0xec, 0x5d, 0xe9, 0x36, 0x3f, 0xe8, 0xae, 0x15, 0x81, 0xdf, 0xec, 0x6a,
	0xbc, 0x80, 0x69, 0xa2, 0x9a, 0xce, 0x92, 0x98, 0xb2, 0xfd, 0x21, 0xe7, 0x33, 0x2a, 0x74, 0xf3,
	0x2a, 0x66, 0xde, 0x7b, 0xc2, 0x08, 0x0e, 0x13, 0x55, 0x55, 0xca, 0x30, 0x1a, 0xb1, 0xc7, 0xdd,
	0x5f, 0x6e, 0x6e, 0xe0, 0x78, 0xf8, 0x52, 0xd6, 0x37, 0x85, 0xbb, 0xca, 0x93, 0x6a, 0x4a, 0x3c,
	0xf5, 0x87, 0x90, 0x43, 0x3b, 0xfc, 0xab, 0xa2, 0x1d, 0xbc, 0x85, 0x59, 0x46, 0x96, 0x0f, 0xfa,
	0xcf, 0xc8, 0x6a, 0xca, 0xee, 0xee, 0x3b, 0x00, 0x00, 0xff, 0xff, 0x5c, 0x12, 0x4c, 0x55, 0xcc,
	0x01, 0x00, 0x00,
}
